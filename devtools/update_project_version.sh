#!/bin/sh

if [ "$1" == -h ] || [ "$1" == --help ]; then
	echo "Usage: ./devtools/update_project_version.sh <VERSION>"
	echo "Modifie la version du projet dans tous les fichiers requis."
	exit
fi

if [ -z "$1" ]; then
	echo "Nouvelle version attendue en paramètre." >&2
	exit 1
fi

VERSION="$1"

FILE_README_MD=README.md
FILE_SRC_PYSV_PY=src/pysvlib.py

FILE_LIST="
	$FILE_README_MD
	$FILE_SRC_PYSV_PY
"

README_TITLE="pysv  $VERSION"
TITLE_LENGTH=${#README_TITLE}
UNDERLINE=$(printf "%${TITLE_LENGTH}s" | sed 's/ /=/g')
sed -i -e "1s/.*/$README_TITLE/" -e "2s/.*/$UNDERLINE/" README.md

sed -i 's/^VERSION = .\+$/VERSION = '\'"$VERSION"\'/ src/pysvlib.py

echo "Version modifiée avec succès vers $VERSION."
echo "Les fichiers suivants ont été modifiés:"
for file in $FILE_LIST; do
	echo "  $file"
done

