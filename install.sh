#!/bin/bash

#-------------------- help()
help()
{
	echo "Usage : ./install [-n|--dry-run] [-h|--help]"
	echo "Installe pysv vers /usr/local/bin et /usr/local/lib."
	echo "Options:"
	echo " -n --dry-run  N'effectue pas l'installation, affiche ce qui devrait être fait"
}

#-------------------- fatal()
fatal()
{
	echo "$1" >&2
	exit 1
}

#-------------------- green()
green()
{
	echo -ne "\e[32m$@\e[0m"
}

#-------------------- bold()
bold()
{
	echo -ne "\e[1m$@\e[0m"
}

#-------------------- copy()
copy()
{
	if [[ -n $dry_run ]]; then
		echo "  $(bold Copie): $1 => $(green $2)"
	else
		cp -v "$1" "$2" || exit 1
	fi
}

#-------------------- link()
link()
{
	if [[ -n $dry_run ]]; then
		echo "  $(bold Lien): $(green $2) -> $1"
	else
		ln -vfs "$1" "$2" || exit 1
	fi
}

#-------------------- make_directory()
make_directory()
{
	if [[ -n $dry_run ]]; then
		echo "  $(bold Création répertoire): $(green $1)"
	else
		mkdir -vp "$1" || exit 1
	fi
}

#-------------------- do_install()
do_install()
{
	echo "Début de l'installation:"
	make_directory $bin

	echo "Copie des fichiers:"
	for subdir in "${shell_wrappers[@]}" default_config; do
		make_directory $lib/$subdir
		for tool in sv cv; do
			copy ./$subdir/$tool.* $lib/$subdir
		done
	done
	for subdir in "${shell_wrappers[@]}"; do
		copy ./$subdir/pysv_load.* $lib/$subdir
	done

	mkdir -p $lib/src
	copy ./src/pysv.py $lib/src/pysv.py
	copy ./src/pysvlib.py $lib/src/pysvlib.py

	echo "Création d'un lien vers l'exécutable:"
	link $lib/src/pysv.py $bin/$projname

	if [[ -z $dry_run ]]; then
		echo -e "\n$projname installé avec succès.\n"
	fi
}



projname=pysv
prefix=/usr/local
bin=$prefix/bin
lib=$prefix/lib/$projname

shell_wrappers=(
	shell_wrappers/bash
	shell_wrappers/fish
)



for arg in "$@"; do
	case $arg in
		-n|--dry-run)
			dry_run=1
			;;
		-h|--help)
			help
			exit
			;;
		*)
			fatal "Paramètre non-reconnu: $arg"
			;;
	esac
done

do_install

